﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DiccionarioLCM.Models
{
    public class Imagenes
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(50)]
        public string Descripcion { get; set; }
        [MaxLength(30)]
        public string Nombre { get; set; }
        [MaxLength(800)]
        public string Ruta { get; set; }
    }

    public class ViewModelImagenes
    {
        //[Required, FileExtensions(Extensions = "jpg",
        //     ErrorMessage = "Solo se pueden ingresar imagenes")]
        public HttpPostedFileBase File { get; set; }
        [Required, MaxLength(50)]
        public string Descripcion { get; set; }

        [Required,MaxLength(30)]
        public string Nombre { get; set; }
    }
}