﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using DiccionarioLCM.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DiccionarioLCM.Context
{
    public class Context:DbContext
    {
        public Context():base("DefaultConnection")
        {
            
        }
        public DbSet<Imagenes> Imagenes { get; set; }
    }
}