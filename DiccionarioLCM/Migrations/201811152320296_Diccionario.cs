namespace DiccionarioLCM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Diccionario : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Imagenes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(maxLength: 50),
                        Nombre = c.String(maxLength: 30),
                        Ruta = c.String(maxLength: 800),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Imagenes");
        }
    }
}
