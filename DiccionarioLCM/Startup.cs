﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DiccionarioLCM.Startup))]
namespace DiccionarioLCM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
