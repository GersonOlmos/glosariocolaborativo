﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DiccionarioLCM.Models;

namespace DiccionarioLCM.Controllers
{
    public class ImagenController : Controller
    {
        Context.Context context = new Context.Context();
        // GET: Imagen
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RegistrarImagen(ViewModelImagenes imagenes)
        {
            if (ModelState.IsValid)
            {
                byte[] bin = new byte[imagenes.File.InputStream.Length];
                imagenes.File.InputStream.Read(bin, 0, (int)imagenes.File.ContentLength);
                var path = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImagenesProductos");
                System.IO.File.WriteAllBytes(path + $"\\{imagenes.File.FileName}", bin);

                Imagenes imagen = new Imagenes()
                {
                    Descripcion = imagenes.Descripcion,
                    Nombre = imagenes.Nombre,
                    Ruta = imagenes.File.FileName
                };
                context.Set<Imagenes>().Add(imagen);
                context.SaveChanges();

            }
            return RedirectToAction("","Home");
        }

    }
}