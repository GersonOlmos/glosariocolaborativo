﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DiccionarioLCM.Models;

namespace DiccionarioLCM.Controllers.api
{
    public class ImagenesController : ApiController
    {
        Context.Context contex = new Context.Context( );
        [HttpGet]
        public List<Imagenes> GetImagenes()
        {
            return contex.Set<Imagenes>().ToList();
        }
    }
}
